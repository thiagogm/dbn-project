import numpy as np
import matplotlib.pyplot as plt
from tensorflow.contrib import learn
import os

# Function for returning a total of `num` random samples and labels.
def next_batch(num, data, labels):
    idx = np.arange(0, len(data))
    np.random.shuffle(idx)
    idx = idx[:num]

    data_shuffle = [data[i] for i in idx]
    labels_shuffle = [labels[i] for i in idx]

    return np.asarray(data_shuffle).T, np.asarray(labels_shuffle)


# Function for returning an "one-hot" style array based on the input data.
def one_hot(data_input):
    maxi = np.amax(data_input, axis=0)
    mini = np.amin(data_input, axis=0)

    size = maxi - mini + 1
    one_hot_input = np.zeros((data_input.shape[0], size[0]))

    for i in range(data_input.shape[0]):
        one_hot_input[i, data_input[i, 0]-mini[0]] = 1

    return one_hot_input


# Split iris data set in train and test sets, considering 3 species of flowers.
def data_split_train_test(data, labels):
    idx_1 = np.arange(0, 50)
    idx_2 = np.arange(50, 100)
    idx_3 = np.arange(100, 150)

    np.random.shuffle(idx_1)
    np.random.shuffle(idx_2)
    np.random.shuffle(idx_3)

    idx_test = np.concatenate((idx_1[25: len(idx_1)], idx_2[25: len(idx_2)], idx_3[25: len(idx_3)]))
    idx_train = np.concatenate((idx_1[:25], idx_2[:25], idx_3[:25]))

    data_shuffle_train = [data[i] for i in idx_train]
    labels_shuffle_train = [labels[i] for i in idx_train]
    data_shuffle_test = [data[i] for i in idx_test]
    labels_shuffle_test = [labels[i] for i in idx_test]

    return np.asarray(data_shuffle_train), np.asarray(labels_shuffle_train),\
        np.asarray(data_shuffle_test), np.asarray(labels_shuffle_test)


# Loading iris data set
def load_iris():
    iris = learn.datasets.load_dataset('iris')

    # normalizing input data for every input variable
    for i in range(4):
        iris.data[:, i] = (iris.data[:, i] - iris.data[:, i].min()) / (iris.data[:, i].max() - iris.data[:, i].min())

    train_inputs, train_labels, test_inputs, test_labels = data_split_train_test(iris.data, iris.target)

    train_labels_one_hot = one_hot(np.reshape(train_labels, (train_labels.shape[0], 1)))
    test_labels_one_hot = one_hot(np.reshape(test_labels, (test_labels.shape[0], 1)))

    return train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot


def system_states(train_inputs, train_labels_one_hot):
    n_classes = train_labels_one_hot.shape[1]
    n_features = train_inputs.shape[1]
    n_elements = train_inputs.shape[0]

    H = np.zeros((n_classes, n_features))
    for i in range(n_classes):
        for f in range(n_elements):
            if np.nonzero(train_labels_one_hot[f, :])[0] == i:
                H[i, :] += train_inputs[f, :]
        H[i, :] = H[i, :]/np.sum(train_labels_one_hot, axis=0)[i]

    return H


def minkowski_distance(H, input_data, sensor_config, alpha):
    n_classes = H.shape[0]
    n_sensors = sensor_config.shape[1]
    D = np.zeros((n_sensors, n_classes))
    n_features = 0
    for i in range(n_sensors):
        for f in range(n_classes):
            for g in range(sensor_config[0, i]):
                D[i, f] += np.power((input_data[n_features + g, 0] - H[f, n_features + g]), alpha)
            D[i, f] = np.power(D[i, f], (1/alpha))
        n_features += sensor_config[0, i]

    return D


def probability_matrix(D):
    D[(D == 0)] = 1E-5
    P = np.power(D, -1)
    np.sum(P, axis=1)
    P_normalized = P/np.repeat(np.reshape(np.sum(P, axis=1),(P.shape[0], 1)), P.shape[1], axis=1)
    return P_normalized


def mass_function(P, W):
    P_weighted = P*W
    m = np.prod(P_weighted, axis=0)/(1 - np.sum(np.prod(P_weighted, axis=0)))
    m = np.reshape(m, (1, m.size))
    m_normalized = m/np.repeat(np.reshape(np.sum(m, axis=1),(m.shape[0], 1)), m.shape[1], axis=1)
    return m_normalized


def fusion_accuracy(value, target):
    accuracy = np.zeros(value.shape)
    accuracy[(value[:, 0:value.shape[1]] == target[:, 0:value.shape[1]])] = 5

    sum_accuracy = np.sum(accuracy, axis=1)
    sum_accuracy[(sum_accuracy != 5 * value.shape[1])] = 0
    sum_accuracy[sum_accuracy == 5 * value.shape[1]] = 1

    return np.sum(sum_accuracy)*100/value.shape[0]


def main():
    for f in range(50):
        sensor_config = np.array([[1,1,1,1]])
        train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot = load_iris()
        H = system_states(train_inputs, train_labels_one_hot)
        # print(train_inputs)
        # print(train_labels_one_hot)
        #print(H)
        #print(sensor_config.shape,sensor_config)
        #print(np.reshape(test_inputs[0],(4,1)).shape, np.reshape(test_inputs[0],(4,1)))
        classification = np.zeros(test_labels_one_hot.shape)
        for i in range(test_inputs.shape[0]):
            D = minkowski_distance(H, np.reshape(test_inputs[i],(4,1)), sensor_config, 2)
            P = probability_matrix(D)
            W = np.ones(P.shape)
            fusion = mass_function(P, W)
            classification[i] = (fusion == fusion.max(axis=1)).astype(int)

        print(fusion_accuracy(classification, test_labels_one_hot))
        # W2 = 0.5*np.ones(P.shape)
        # print(mass_function(P,W2))


if __name__ == '__main__':
    main()

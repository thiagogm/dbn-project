import numpy as np
# import tensorflow as tf
from tensorflow.contrib import learn


# Function for returning a total of `num` random samples and labels.
def next_batch(num, data, labels):
    idx = np.arange(0, len(data))
    np.random.shuffle(idx)
    idx = idx[:num]
    data_shuffle = [data[i] for i in idx]
    labels_shuffle = [labels[i] for i in idx]
    return np.asarray(data_shuffle).T, np.asarray(labels_shuffle).T

def one_hot(data_input):
    max = np.amax(data_input, axis=0)
    min = np.amin(data_input, axis=0)
    size = max - min + 1
    one_hot = np.zeros((data_input.shape[0], size[0]))
    for i in range(data_input.shape[0]):
        one_hot[i,data_input[i,0]-min[0]] = 1
    return one_hot

# Loading data
iris = learn.datasets.load_dataset('iris')

train_inputs = np.concatenate((iris.data[0:25], iris.data[50:75], iris.data[100:125]), axis=0)
train_labels = np.concatenate((iris.target[0:25], iris.target[50:75], iris.target[100:125]))

test_inputs = np.concatenate((iris.data[25:50], iris.data[75:100], iris.data[125:150]), axis=0)
test_labels = np.concatenate((iris.target[25:50], iris.target[75:100], iris.target[125:150]))


def entropy_measure(degree_of_membership):
    # def entropy_measure(input_data, mean, variance):
    # input is a [i,j] matrix.
    # this function returns a float value.
    # output_layer_2 = layer_2(input_data, mean, variance)
    degree_of_membership = degree_of_membership.astype(float)
    degree_of_non_membership = 1 - degree_of_membership
    eps = 1e-50
    degree_of_non_membership[degree_of_non_membership < eps] = eps
    # output_layer_1_rep = np.repeat(input_data, mean.shape[1], axis=1)
    # This next formula may be wrong in the paper!! Here it is corrected.
    # test = (-np.power((output_layer_1_rep - mean), 2) / (2 * np.power(variance, 2)))
    # D = np.exp(np.power(u, -1)) # This equation may be wrong in the paper.
    # EM = -D*np.log2(D)          # Consider the new one using degree of membership and degree of non-membership.
    # em = -(degree_of_membership*(test)/np.log(2) + (1 - degree_of_membership)*(np.log(-test)+(test/2)+(np.power(test,2)/24)-(np.power(test,4)/2880))/np.log(2))
    # em = -(degree_of_membership*np.log2(degree_of_membership)+(1-degree_of_membership)*np.log2(1-degree_of_membership))
    em = -(degree_of_membership*np.log2(degree_of_membership)+degree_of_non_membership*np.log2(degree_of_non_membership))
    return (em.sum(0)/degree_of_membership.shape[0]).max()


def functional_expansion(input_variables):
    # input is a [i,1] matrix.
    # basis is a [3*i,1] or [k,1] matrix.
    basis = np.array([[input_variables[0, 0],
                       np.sin(np.pi*input_variables[0, 0]),
                       np.cos(np.pi*input_variables[0, 0])]]).T
    for i in range(1, input_variables.shape[0]):
        basis = np.concatenate((basis, np.array([[input_variables[i, 0],
                                                  np.sin(np.pi*input_variables[i, 0]),
                                                  np.cos(np.pi*input_variables[i, 0])]]).T), axis=0)
    return basis


def flnn_output(weights, basis):
    # weights is a [k,j] matrix.
    # basis is a [k,1] matrix.
    # this function returns a [1,j] matrix.
    return np.matmul(weights.T, basis).T


def layer_2(output_layer_1, mean, variance):
    # input is a [i,1] matrix.
    # mean is a [i,j] matrix.
    # variance is a [i,j] matrix.
    # this function returns a [i,j] matrix.
    output_layer_1_rep = np.repeat(output_layer_1, mean.shape[1], axis=1)
    # This next formula may be wrong in the paper!! Here it is corrected.
    return np.exp(-np.power((output_layer_1_rep-mean), 2)/(2*np.power(variance, 2)))


def layer_3(output_layer_2):
    # output_layer_2 is a [i,j] matrix.
    # output is a [1,j] matrix.
    output = np.zeros((1, output_layer_2.shape[1]))
    for j in range(0, output_layer_2.shape[1]):
        output[0, j] = np.prod(output_layer_2[:, j])
    return output


def layer_4(output_layer_3, output_flnn):
    # output_layer_3 is a [1,j] matrix.
    # output_flnn is a [1,j] matrix.
    # this function returns a [1,j] matrix.
    return output_layer_3*output_flnn


def layer_5(output_layer_3, output_layer_4):
    # output_layer_3 is a [1,j] matrix.
    # output_layer_4 is a [1,j] matrix.
    # this function returns a value.
    return np.sum(output_layer_4, axis=1)/np.sum(output_layer_3, axis=1)


def evaluate(input_data, weights, mean, variance):
    basis = functional_expansion(input_data)
    flnn = flnn_output(weights, basis)
    output_layer_2 = layer_2(input_data, mean, variance)
    output_layer_3 = layer_3(output_layer_2)
    output_layer_4 = layer_4(output_layer_3, flnn)
    output_layer_5 = layer_5(output_layer_3, output_layer_4)
    return output_layer_5


def train_bp(train_data, output_desired, weights, mean, variance, eta_weights, eta_mean, eta_variance):
    # train_data is a [i,1] matrix.
    # mean is a [i,j] matrix.
    # variance is a [i,j] matrix.
    # output_desired is a value.
    # d_weights is a [k,j] matrix.
    # d_mean is a [i,j] matrix.
    # d_variance is a [i,j] matrix.
    # etas weight, mean and variance are values.

    basis = functional_expansion(train_data)
    flnn = flnn_output(weights, basis)
    output_layer_2 = layer_2(train_data, mean, variance)
    output_layer_3 = layer_3(output_layer_2)
    output_layer_4 = layer_4(output_layer_3, flnn)
    output_layer_5 = layer_5(output_layer_3, output_layer_4)
    error = (output_layer_5 - output_desired)
    d_weights = -eta_weights*error*((np.matmul(basis, output_layer_3))/np.sum(output_layer_3, axis=1))
    output_layer_3_sum = np.sum(output_layer_3, axis=1)
    output_layer_4_rep = np.repeat(output_layer_4, train_data.shape[0], axis=0)
    train_data_rep = np.repeat(train_data, weights.shape[1], axis=1)
    # The next equation may be wrong in the paper. Here an extra 2 factor was removed.
    d_mean = -eta_mean*error*(output_layer_4_rep/output_layer_3_sum)*((train_data_rep - mean)/np.power(variance, 2))
    # This next equation may be wrong in the paper. Here an extra 2 factor was removed.
    d_variance = -eta_variance*error*(
        output_layer_4_rep/output_layer_3_sum)*(np.power((train_data_rep - mean), 2)/np.power(variance, 3))
    return weights+d_weights, mean+d_mean, variance+d_variance


def structure_learning(input_total, output_total):
    sigma_init = 0.5
    eta_weights = 0.01
    eta_mean = 0.01
    eta_variance = 0.01
    # tal = 0.28
    em_mean = 0.25 # tal*input_total.shape[0]
    i = 0
    error = 1.0

    weights = np.zeros((0, 1))
    mean = np.zeros((0, 1))
    variance = np.zeros((0, 1))
    while error > 0.00001 or i < 10000:
        print(weights.shape, mean.shape, variance.shape)
        input_batch, output_desired = next_batch(1, input_total, output_total)
        # print('input = %s' %input)
        # print('input shape =', input.shape)
        # print('output = %s' %output_desired)
        # print('output shape=', output_desired.shape)
        if weights.shape[0] == 0:
            weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3 * input_batch.shape[0], 1))), axis=0)
            mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=0)
            variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=0)
            print(input_batch)
            print(weights)
            print(mean)
            print(variance)
        else:
            output_layer_2 = layer_2(input_batch, mean, variance)
            em_max = entropy_measure(output_layer_2)
            # em_max = entropy_measure(input_batch, mean, variance)
            print('EM max', em_max)
            if em_max < em_mean:
                weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3*input_batch.shape[0], 1))), axis=1)
                mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=1)
                variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=1)

        error = np.power((evaluate(input_batch, weights, mean, variance) - output_desired), 2)/2
        print('output =', evaluate(input_batch, weights, mean, variance))
        print('desired', output_desired)
        print('error = %s' % error)
        if error > 0.00001:
            weights, mean, variance = train_bp(input_batch, output_desired, weights, mean, variance,
                                               eta_weights, eta_mean, eta_variance)
        i = i + 1
        print('iteration = %s' % i)

    # print('weights =', weights)
    # print('mean =', mean)
    # print('variance =', variance)
    return weights, mean, variance


def main():

    # p = np.array([[1],[2]])
    # q = np.array([[3,2]])
    # q2 = np.array([[3,5]])
    # u = np.array([[1,2,3],[0.5,5,6]])
    # u2 = np.array([[0.1,0.2,0.3],[0.5,0.5,0.6]])
    # u3 = np.array([[0, 0, 1, 1], [0, 1, 0, 1]])
    # u4 = np.array([[0, 1, 1, 1]])
    # v = np.array([[1,1,1],[1,1,2]])
    # w = np.random.rand(2,3)
    # a = np.zeros((0,1))
    # a = np.concatenate((a, np.random.rand(3 * a.shape[0], 1)),axis=0)
    # a = np.concatenate((a,np.array([[2],[3]])),axis=1)
    # weights = np.zeros((6,2))
    # weights[:, 1] = np.random.rand(3 * p.shape[0], 1)
    # batch_images, batch_labels = next_batch(1, u3.T, u4.T)

    train_labelss = np.reshape(train_labels, (train_labels.shape[0], 1))
    weights, mean, variance = structure_learning(train_inputs, train_labelss)
    print(test_inputs[10])
    results = np.zeros((75, 2))
    for h in range(75):
        results[h, 0] = evaluate(np.reshape(test_inputs[h], (4, 1)), weights, mean, variance)
        results[h, 1] = test_labels[h]

    # print(np.rint(results))
    print((results))
    #print(one_hot(train_labelss + 1), train_labelss+1)

if __name__ == '__main__':
    main()

# import numpy as np
# import math as m
# from tensorflow.contrib import learn
#
#
# # Function for returning a total of `num` random samples and labels.
# def next_batch(num, data, labels):
#     idx = np.arange(0, len(data))
#     np.random.shuffle(idx)
#     idx = idx[:num]
#
#     data_shuffle = [data[i] for i in idx]
#     labels_shuffle = [labels[i] for i in idx]
#
#     return np.asarray(data_shuffle).T, np.asarray(labels_shuffle)
#
#
# def one_hot(data_input):
#     maxi = np.amax(data_input, axis=0)
#     mini = np.amin(data_input, axis=0)
#
#     size = maxi - mini + 1
#     one_hot_input = np.zeros((data_input.shape[0], size[0]))
#
#     for i in range(data_input.shape[0]):
#         one_hot_input[i, data_input[i, 0]-mini[0]] = 1
#
#     return one_hot_input
#
#
# # Split iris data set in train and test sets, considering 3 species of flower
# def data_split_train_test(data, labels):
#     idx_1 = np.arange(0, 50)
#     idx_2 = np.arange(50, 100)
#     idx_3 = np.arange(100, 150)
#
#     np.random.shuffle(idx_1)
#     np.random.shuffle(idx_2)
#     np.random.shuffle(idx_3)
#
#     idx_test = np.concatenate((idx_1[25: len(idx_1)], idx_2[25: len(idx_2)], idx_3[25: len(idx_3)]))
#     idx_train = np.concatenate((idx_1[:25], idx_2[:25], idx_3[:25]))
#
#     data_shuffle_train = [data[i] for i in idx_train]
#     labels_shuffle_train = [labels[i] for i in idx_train]
#     data_shuffle_test = [data[i] for i in idx_test]
#     labels_shuffle_test = [labels[i] for i in idx_test]
#
#     return np.asarray(data_shuffle_train), np.asarray(labels_shuffle_train),\
#         np.asarray(data_shuffle_test), np.asarray(labels_shuffle_test)
#
#
# # Loading data
# def load_iris():
#     iris = learn.datasets.load_dataset('iris')
#     for i in range(4):
#         iris.data[:, i] = (iris.data[:, i] - iris.data[:, i].min()) / (iris.data[:, i].max() - iris.data[:, i].min())
#
#     train_inputs, train_labels, test_inputs, test_labels = data_split_train_test(iris.data, iris.target)
#
#     train_labels_one_hot = one_hot(np.reshape(train_labels, (train_labels.shape[0], 1)))
#     test_labels_one_hot = one_hot(np.reshape(test_labels, (test_labels.shape[0], 1)))
#
#     return train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot
#
#
# def entropy_measure(degree_membership):
#     # def entropy_measure(input_data, mean, variance):
#     # input is a [i,j] matrix.
#     # this function returns a float value.
#     # output_layer_2 = layer_2(input_data, mean, variance)
#     degree_membership = degree_membership.astype(float)
#     degree_non_membership = 1 - degree_membership
#
#     eps = 1e-25
#     degree_membership[degree_membership < eps] = eps
#     degree_non_membership[degree_non_membership < eps] = eps
#
#     em = -(degree_membership*np.log2(degree_membership)+degree_non_membership*np.log2(degree_non_membership))
#
#     return (em.sum(0)/degree_membership.shape[0]).max()
#
#
# def functional_expansion(input_variables):
#     # input is a [i,1] matrix.
#     # basis is a [3*i,1] or [k,1] matrix.
#     basis = np.array([[input_variables[0, 0],
#                        np.sin(np.pi*input_variables[0, 0]),
#                        np.cos(np.pi*input_variables[0, 0])]]).T
#
#     for i in range(1, input_variables.shape[0]):
#         basis = np.concatenate((basis, np.array([[input_variables[i, 0],
#                                                   np.sin(np.pi*input_variables[i, 0]),
#                                                   np.cos(np.pi*input_variables[i, 0])]]).T), axis=0)
#
#     return basis
#
#
# def flnn_output(weights, basis):
#     # weights is a [k,j,l] matrix.
#     # basis is a [k,1] matrix.
#     # output is  a [1,j,l] matrix.
#     output = np.zeros((1, weights.shape[1], weights.shape[2]))
#     weights = np.transpose(weights, axes=[1, 0, 2])
#
#     for i in range(weights.shape[2]):
#         output[0, :, i] = np.matmul(weights[:, :, i], basis).T
#
#     return output
#
#
# def layer_2(output_layer_1, mean, variance):
#     # input is a [i,1] matrix.
#     # mean is a [i,j] matrix.
#     # variance is a [i,j] matrix.
#     # this function returns a [i,j] matrix.
#     output_layer_1_rep = np.repeat(output_layer_1, mean.shape[1], axis=1)
#
#     # This next formula may be wrong in the paper!! Here it is corrected.
#     return np.exp(-np.power((output_layer_1_rep-mean), 2)/(2*np.power(variance, 2)))
#
#
# def layer_3(output_layer_2):
#     # output_layer_2 is a [i,j] matrix.
#     # output is a [1,j] matrix.
#     output = np.zeros((1, output_layer_2.shape[1]))
#
#     for j in range(0, output_layer_2.shape[1]):
#         output[0, j] = np.prod(output_layer_2[:, j])
#
#     return output
#
#
# def layer_4(output_layer_3, output_flnn):
#     # output_layer_3 is a [1,j] matrix.
#     # output_flnn is a [1,j,l] matrix.
#     # this function returns a [1,j,l] matrix.
#     shape_3 = output_layer_3.shape
#     output_layer_3_rep = np.repeat(np.reshape(output_layer_3, [shape_3[0], shape_3[1], 1]),
#                                    output_flnn.shape[2], axis=2)
#
#     return output_layer_3_rep*output_flnn
#
#
# def layer_5(output_layer_3, output_layer_4):
#     # output_layer_3 is a [1,j] matrix.
#     # output_layer_4 is a [1,j,l] matrix.
#     # this function returns a [1,l] matrix.
#     return np.sum(output_layer_4, axis=1)/np.sum(output_layer_3, axis=1)
#
#
# def evaluate_mfnfn(input_data, weights, mean, variance):
#     basis = functional_expansion(input_data)
#     flnn = flnn_output(weights, basis)
#     output_layer_2 = layer_2(input_data, mean, variance)
#     output_layer_3 = layer_3(output_layer_2)
#     output_layer_4 = layer_4(output_layer_3, flnn)
#     output_layer_5 = layer_5(output_layer_3, output_layer_4)
#
#     return output_layer_5
#
#
# def train_bp(train_data, output_desired, weights, mean, variance, eta_weights, eta_mean, eta_variance):
#     # train_data is a [i,1] matrix.
#     # weights is a [k,j,l] matrix.
#     # mean is a [i,j] matrix.
#     # variance is a [i,j] matrix.
#     # output_desired is a [1,l] matrix.
#     # d_weights is a [k,j,l] matrix.
#     # d_mean is a [i,j] matrix.
#     # d_variance is a [i,j] matrix.
#     # etas weight, mean and variance are values.
#
#     basis = functional_expansion(train_data)
#     flnn = flnn_output(weights, basis)
#     output_layer_2 = layer_2(train_data, mean, variance)
#     output_layer_3 = layer_3(output_layer_2)
#     output_layer_4 = layer_4(output_layer_3, flnn)
#     output_layer_5 = layer_5(output_layer_3, output_layer_4)
#     error = np.zeros((weights.shape[0], weights.shape[1], weights.shape[2]))
#     for i in range(weights.shape[2]):
#         error[:, :, i] = -(output_desired[0, i] - output_layer_5[0, i])
#     d_weights = -eta_weights*error*(np.repeat(np.reshape(((np.matmul(basis, output_layer_3))/np.sum(
#         output_layer_3, axis=1)), [basis.shape[0], output_layer_3.shape[1], 1]), weights.shape[2], axis=2))
#
#     output_layer_3_sum = np.sum(output_layer_3, axis=1)
#     output_layer_4_rep = np.repeat(output_layer_4, train_data.shape[0], axis=0)
#     train_data_rep = np.repeat(train_data, weights.shape[1], axis=1)
#     # The next equation may be wrong in the paper. Here an extra 2 factor was removed.
#     error_2 = np.zeros((1, weights.shape[1], weights.shape[2]))
#     for i in range(weights.shape[2]):
#         error_2[0, :, i] = -(output_desired[0, i] - output_layer_5[0, i])
#     d_mean = -eta_mean*(np.sum((error_2*output_layer_4_rep), axis=2)/output_layer_3_sum)*((train_data_rep -
#                                                                                            mean)/np.power(variance, 2))
#     # This next equation may be wrong in the paper. Here an extra 2 factor was removed.
#     d_variance = -eta_variance*(np.sum((error_2*output_layer_4_rep), axis=2)/output_layer_3_sum)*(
#         np.power((train_data_rep - mean), 2)/np.power(variance, 3))
#     return weights+d_weights, mean+d_mean, variance+d_variance
#
#
# def structure_learning(input_total, output_total):
#     sigma_init = 0.5
#     eta_weights = 0.01
#     eta_mean = 0.01
#     eta_variance = 0.01
#     # tal = 0.28
#     em_mean = 0.24  # tal*input_total.shape[0]
#     i = 0
#     error = 1.0
#
#     weights = np.zeros((0, 1, output_total.shape[1]))
#     mean = np.zeros((0, 1))
#     variance = np.zeros((0, 1))
#     while error > 0.00001 or i < 10000:
#         print(weights.shape, mean.shape, variance.shape)
#         input_batch, output_desired = next_batch(1, input_total, output_total)
#         if weights.shape[0] == 0:
#             weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3 * input_batch.shape[0], 1,
#                                                                               output_total.shape[1]))), axis=0)
#             mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=0)
#             variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=0)
#             print(weights)
#             print(mean)
#             print(variance)
#         else:
#             output_layer_2 = layer_2(input_batch, mean, variance)
#             em_max = entropy_measure(output_layer_2)
#             print('EM max', em_max)
#             if em_max < em_mean:
#                 weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3*input_batch.shape[0], 1,
#                                                                                   output_total.shape[1]))), axis=1)
#                 mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=1)
#                 variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=1)
#
#         error = np.sum((np.power((output_desired - evaluate_mfnfn(input_batch, weights, mean, variance)), 2)/2), axis=1)
#         print('output =', evaluate_mfnfn(input_batch, weights, mean, variance))
#         print('desired', output_desired)
#         print('error = %s' % error)
#         if error > 0.00001:
#             weights, mean, variance = train_bp(input_batch, output_desired, weights, mean, variance,
#                                                eta_weights, eta_mean, eta_variance)
#         i = i + 1
#         print('iteration = %s' % i)
#
#     # print('weights =', weights)
#     # print('mean =', mean)
#     # print('variance =', variance)
#     return weights, mean, variance
#
#
# def grouping(g, gr, n):
#     layers = n - (gr - 1)
#     r = 0
#     for count in range(layers):
#         if gr > 1:
#             r = r + g[0, count]*grouping(np.reshape(g[0, (count+1):g.shape[1]], (1, g[0, (count+1):g.shape[1]].size)), gr-1, n-(count+1))
#         else:
#             r = r + g[0, count]
#     return r
#
#
# def lambda_calc(g):
#     p = np.zeros(g.shape[1])
#     for i in range(g.shape[1]):
#         p[g.shape[1] - i - 1] = grouping(g, i+1, g.shape[1])
#     p[p.size - 1] = p[p.size - 1] - 1
#     roots = np.roots(p)
#     for i in range(roots.size):
#         if ~isinstance(roots[i], complex):
#             if roots[i] > -1:
#                 lambda_value = roots[i]
#     return lambda_value
#
#
# def main():
#     train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot = load_iris()
#     weights, mean, variance = structure_learning(train_inputs, train_labels_one_hot)
#     results = np.zeros((75, 6))
#     results2 = np.zeros((75, 3))
#     for h in range(75):
#         results[h, 0:3] = evaluate_mfnfn(np.reshape(test_inputs[h], (4, 1)), weights, mean, variance)
#         # results[h, 3:6] = (results[h, 0:3] - np.min(results[h, 0:3]))/(np.max(results[h, 0:3])- np.min(results[h, 0:3]))
#         results[h, 3:6] = test_labels_one_hot[h]
#         results2[h, 0:3] = np.absolute(1 - np.absolute(results[h, 0:3] - 1))
#
#     print(results2)
#     results = np.absolute(np.rint(results))
#     accuracy = np.zeros((75, 3))
#     accuracy[(results[:, 0:3] == results[:, 3:6])] = 5
#     print(results)
#     summ = np.sum(accuracy, axis=1)
#     summ[(summ != 15)] = 0
#     summ[summ == 15] = 1
#     print(np.sum(summ)*100/75)
#     g = np.array([[np.sum(summ[0:25])/25,np.sum(summ[25:50])/25,np.sum(summ[50:75])/25]])/2
#     print(g)
#     lambda_value = lambda_calc(g)
#     print(lambda_value)
#
#
# if __name__ == '__main__':
#     main()

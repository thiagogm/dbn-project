# import numpy as np
# import matplotlib.pyplot as plt
# from tensorflow.contrib import learn
# import os
#
#
# # Function for returning a total of `num` random samples and labels.
# def next_batch(num, data, labels):
#     idx = np.arange(0, len(data))
#     np.random.shuffle(idx)
#     idx = idx[:num]
#
#     data_shuffle = [data[i] for i in idx]
#     labels_shuffle = [labels[i] for i in idx]
#
#     return np.asarray(data_shuffle).T, np.asarray(labels_shuffle)
#
#
# # Function for returning an "one-hot" style array based on the input data.
# def one_hot(data_input):
#     maxi = np.amax(data_input, axis=0)
#     mini = np.amin(data_input, axis=0)
#
#     size = maxi - mini + 1
#     one_hot_input = np.zeros((data_input.shape[0], size[0]))
#
#     for i in range(data_input.shape[0]):
#         one_hot_input[i, data_input[i, 0]-mini[0]] = 1
#
#     return one_hot_input
#
#
# # Split iris data set in train and test sets, considering 3 species of flowers.
# def data_split_train_test(data, labels):
#     idx_1 = np.arange(0, 50)
#     idx_2 = np.arange(50, 100)
#     idx_3 = np.arange(100, 150)
#
#     np.random.shuffle(idx_1)
#     np.random.shuffle(idx_2)
#     np.random.shuffle(idx_3)
#
#     idx_test = np.concatenate((idx_1[25: len(idx_1)], idx_2[25: len(idx_2)], idx_3[25: len(idx_3)]))
#     idx_train = np.concatenate((idx_1[:25], idx_2[:25], idx_3[:25]))
#
#     data_shuffle_train = [data[i] for i in idx_train]
#     labels_shuffle_train = [labels[i] for i in idx_train]
#     data_shuffle_test = [data[i] for i in idx_test]
#     labels_shuffle_test = [labels[i] for i in idx_test]
#
#     return np.asarray(data_shuffle_train), np.asarray(labels_shuffle_train),\
#         np.asarray(data_shuffle_test), np.asarray(labels_shuffle_test)
#
#
# # Loading iris data set
# def load_iris():
#     iris = learn.datasets.load_dataset('iris')
#     # normalizing input data for every input variable
#     for i in range(4):
#         iris.data[:, i] = (iris.data[:, i] - iris.data[:, i].min()) / (iris.data[:, i].max() - iris.data[:, i].min())
#
#     train_inputs, train_labels, test_inputs, test_labels = data_split_train_test(iris.data, iris.target)
#
#     train_labels_one_hot = one_hot(np.reshape(train_labels, (train_labels.shape[0], 1)))
#     test_labels_one_hot = one_hot(np.reshape(test_labels, (test_labels.shape[0], 1)))
#
#     return train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot
#
#
# # Function to calculate the entropy measure of the neural network.
# def entropy_measure(degree_membership):
#     # degree_membership is a [i,j] matrix.
#     # this function returns a float value.
#     degree_membership = degree_membership.astype(float)
#     degree_non_membership = 1 - degree_membership
#
#     eps = 1e-25
#     degree_membership[degree_membership < eps] = eps
#     degree_non_membership[degree_non_membership < eps] = eps
#
#     em = -(degree_membership*np.log2(degree_membership) + degree_non_membership*np.log2(degree_non_membership))  # change to ln?!
#
#     return (np.sum(em, axis=0)/degree_membership.shape[0]).max()
#
#
# # Function to perform a functional expansion in the form f(x) = [x, sin(πx), cos(πx)].
# def functional_expansion(input_variables):
#     # input is a [i,1] matrix.
#     # basis is a [k ,1] (or [3*i,1]) matrix.
#     basis = np.array([[input_variables[0, 0],
#                        np.sin(np.pi*input_variables[0, 0]),
#                        np.cos(np.pi*input_variables[0, 0])]]).T
#
#     for i in range(1, input_variables.shape[0]):
#         basis = np.concatenate((basis, np.array([[input_variables[i, 0],
#                                                   np.sin(np.pi*input_variables[i, 0]),
#                                                   np.cos(np.pi*input_variables[i, 0])]]).T), axis=0)
#
#     return basis
#
#
# # Function to calculate the output from the Functional Link Neural Network.
# def flnn_output(weights, basis):
#     # weights is a [k,j,l] matrix.
#     # basis is a [k,1] matrix.
#     # output is  a [1,j,l] matrix.
#     output = np.zeros((1, weights.shape[1], weights.shape[2]))
#     weights = np.transpose(weights, axes=[1, 0, 2])
#
#     for i in range(weights.shape[2]):
#         output[0, :, i] = np.matmul(weights[:, :, i], basis).T
#
#     return output
#
#
# # Function to calculate the output from the second layer of the Functional Neural Fuzzy Networks.
# def layer_2(output_layer_1, mean, variance):
#     # input is a [i,1] matrix.
#     # mean is a [i,j] matrix.
#     # variance is a [i,j] matrix.
#     # this function returns a [i,j] matrix.
#     output_layer_1_rep = np.repeat(output_layer_1, mean.shape[1], axis=1)
#
#     # This next formula may be wrong in the paper!! Here it is corrected.
#     return np.exp(-np.power((output_layer_1_rep-mean), 2)/(2*np.power(variance, 2)))
#
#
# # Function to calculate the output from the third layer of the Functional Neural Fuzzy Networks.
# def layer_3(output_layer_2):
#     # output_layer_2 is a [i,j] matrix.
#     # output is a [1,j] matrix.
#     output = np.zeros((1, output_layer_2.shape[1]))
#
#     for j in range(0, output_layer_2.shape[1]):
#         output[0, j] = np.prod(output_layer_2[:, j])
#
#     return output
#
#
# # Function to calculate the output from the fourth layer of the Functional Neural Fuzzy Networks.
# # This is the point where the FLNN and the FNFN connect.
# def layer_4(output_layer_3, output_flnn):
#     # output_layer_3 is a [1,j] matrix.
#     # output_flnn is a [1,j,l] matrix.
#     # this function returns a [1,j,l] matrix.
#     shape_3 = output_layer_3.shape
#     output_layer_3_rep = np.repeat(np.reshape(output_layer_3, [shape_3[0], shape_3[1], 1]),
#                                    output_flnn.shape[2], axis=2)
#
#     return output_layer_3_rep*output_flnn
#
#
# # Function to calculate the output from the fifth layer of the Functional Neural Fuzzy Networks.
# def layer_5(output_layer_3, output_layer_4):
#     # output_layer_3 is a [1,j] matrix.
#     # output_layer_4 is a [1,j,l] matrix.
#     # this function returns a [1,l] matrix.
#     if np.sum(output_layer_3, axis=1) > 0:
#         return np.sum(output_layer_4, axis=1)/np.sum(output_layer_3, axis=1)
#     else:
#         return np.sum(output_layer_4, axis=1)/1e-10
#
#
# # Function to calculate the output from the sixth layer of the Functional Neural Fuzzy Networks.
# # Applying Softmax classification.
# def layer_6(output_layer_5):
#     # output_layer_5 is a [1,l] matrix.
#     # this function returns a [1,l] matrix.
#     return np.exp(output_layer_5)/np.sum(np.exp(output_layer_5))
#
#
# # Function to iterate forward through all the layers of the FNFN (including FLNN).
# def evaluate_fnfn(input_data, weights, mean, variance):
#     basis = functional_expansion(input_data)
#     flnn = flnn_output(weights, basis)
#     output_layer_2 = layer_2(input_data, mean, variance)
#     output_layer_3 = layer_3(output_layer_2)
#     output_layer_4 = layer_4(output_layer_3, flnn)
#     output_layer_5 = layer_5(output_layer_3, output_layer_4)
#     output_layer_6 = layer_6(output_layer_5)
#     return output_layer_6
#
#
# # Function to perform back propagation training in the learnable parameters of the FNFN and FLNN.
# # Learnable parameters are FLNN weights and FNFN means and variances.
# def train_bp(train_data, output_desired, weights, mean, variance, eta_weights, eta_mean, eta_variance):
#     # train_data is a [i,1] matrix.
#     # weights is a [k,j,l] matrix.
#     # mean is a [i,j] matrix.
#     # variance is a [i,j] matrix.
#     # output_desired is a [1,l] matrix.
#     # d_weights is a [k,j,l] matrix.
#     # d_mean is a [i,j] matrix.
#     # d_variance is a [i,j] matrix.
#     # etas weight, mean and variance are values.
#
#     basis = functional_expansion(train_data)
#     flnn = flnn_output(weights, basis)
#     output_layer_2 = layer_2(train_data, mean, variance)
#     output_layer_3 = layer_3(output_layer_2)
#     output_layer_4 = layer_4(output_layer_3, flnn)
#     output_layer_5 = layer_5(output_layer_3, output_layer_4)
#     output_layer_6 = layer_6(output_layer_5)
#     error = np.zeros(weights.shape)
#
#     if np.sum(output_layer_3, axis=1) > 0:
#         output_layer_3_sum = np.sum(output_layer_3, axis=1)
#     else:
#         output_layer_3_sum = 1e-10
#
#     for i in range(weights.shape[2]):
#         error[:, :, i] = output_layer_6[0, i] - output_desired[0, i]
#
#     d_weights = -eta_weights*error*(np.repeat(np.reshape(((np.matmul(basis, output_layer_3))/output_layer_3_sum),
#                                     [basis.shape[0], output_layer_3.shape[1], 1]), weights.shape[2], axis=2))
#
#     output_layer_4_rep = np.repeat(output_layer_4, train_data.shape[0], axis=0)
#     train_data_rep = np.repeat(train_data, weights.shape[1], axis=1)
#
#     # The next equation may be wrong in the paper. Here an extra 2 factor was removed.
#     error_2 = np.zeros((1, weights.shape[1], weights.shape[2]))
#
#     for i in range(weights.shape[2]):
#         error_2[0, :, i] = output_layer_6[0, i] - output_desired[0, i]
#
#     d_mean = -eta_mean*(np.sum((error_2*output_layer_4_rep), axis=2)/output_layer_3_sum)*((train_data_rep -
#                                                                                            mean)/np.power(variance, 2))
#     # This next equation may be wrong in the paper. Here an extra 2 factor was removed.
#     d_variance = -eta_variance*(np.sum((error_2*output_layer_4_rep), axis=2)/output_layer_3_sum)*(
#         np.power((train_data_rep - mean), 2)/np.power(variance, 3))
#
#     return weights+d_weights, mean+d_mean, variance+d_variance
#
#
# # Function to perform the structure learning for the FNFN based on the entropy measure of the system.
# def structure_learning(input_total, output_total):
#     sigma_init = 0.3
#     eta_weights = 0.01
#     eta_mean = 0.01
#     eta_variance = 0.01
#     # tal = 0.28
#     em_mean = 0.24  # tal*input_total.shape[0]
#     em_max = 0
#     i = 0
#     error = 1.0
#
#     weights = np.zeros((0, 1, output_total.shape[1]))
#     mean = np.zeros((0, 1))
#     variance = np.zeros((0, 1))
#     while np.absolute(error) > 0.0001 and i < 1000:
#         input_batch, output_desired = next_batch(1, input_total, output_total)
#         if weights.shape[0] == 0:
#             weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3 * input_batch.shape[0], 1,
#                                                                               output_total.shape[1]))), axis=0)
#             # mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=0)
#             mean = np.concatenate((mean, input_batch), axis=0)
#             variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=0)
#             print(weights)
#             print(mean)
#             print(variance)
#
#         else:
#             output_layer_2 = layer_2(input_batch, mean, variance)
#             em_max = entropy_measure(output_layer_2)
#
#             if em_max < em_mean:
#                 weights = np.concatenate((weights, np.random.uniform(-1, 1, size=(3*input_batch.shape[0], 1,
#                                                                                   output_total.shape[1]))), axis=1)
#                 # mean = np.concatenate((mean, np.repeat([[input_batch[0, 0]]], input_batch.shape[0], axis=0)), axis=1)
#                 mean = np.concatenate((mean, input_batch), axis=1)
#                 variance = np.concatenate((variance, np.repeat([[sigma_init]], input_batch.shape[0], axis=0)), axis=1)
#
#         error = np.sum(-(output_desired*np.log(evaluate_fnfn(input_batch, weights, mean, variance))), axis=1)
#
#         if np.absolute(error) > 0.0001:
#             weights, mean, variance = train_bp(input_batch, output_desired, weights, mean, variance,
#                                                eta_weights, eta_mean, eta_variance)
#         i = i + 1
#         if i % 100 == 0:
#             print(weights.shape, mean.shape, variance.shape)
#             print('EM max', em_max)
#             print('output =', evaluate_fnfn(input_batch, weights, mean, variance))
#             print('desired', output_desired)
#             print('error = %s' % error)
#             print('iteration = %s' % i)
#
#     # print('weights =', weights)
#     # print('mean =', mean)
#     # print('variance =', variance)
#     return weights, mean, variance
#
#
# # Function to calculate binomial coefficient for solving λ-polynomial equation.
# def grouping(g, gr, n):
#     layers = n - (gr - 1)
#     r = 0
#     for count in range(layers):
#         if gr > 1:
#             r = r + g[0, count]*grouping(np.reshape(g[0, (count+1):g.shape[1]],
#                                          (1, g[0, (count+1):g.shape[1]].size)),
#                                          gr-1, n-(count+1))
#         else:
#             r = r + g[0, count]
#     return r
#
#
# # Function to solve λ-polynomial equation and return unique root bigger than -1.
# def lambda_calc(g):
#     lambda_value = -0.9999999
#     p = np.zeros(g.shape[1])
#     for i in range(g.shape[1]):
#         p[g.shape[1] - i - 1] = grouping(g, i+1, g.shape[1])
#     p[p.size - 1] = p[p.size - 1] - 1
#     roots = np.roots(p)
#     for i in range(roots.size):
#         if ~isinstance(roots[i], complex):
#             if roots[i] > -1:
#                 lambda_value = roots[i]
#     return lambda_value
#
#
# def calc_g(output, target):
#     p = np.zeros((target.shape[1], target.shape[1]))
#     n_elements_class = np.sum(target, axis=0)
#     g = np.zeros((1, target.shape[1]))
#
#     for i in range(target.shape[0]):
#         if np.array_equal(output[i, :], target[i, :]):
#             n = np.argmax(target[i, :])
#             p[n, n] = p[n, n] + 1/n_elements_class[n]
#         else:
#             n = np.argmax(target[i, :])
#             m = np.argmax(output[i, :])
#             p[n, m] = p[n, m] + 1/n_elements_class[n]
#
#     for j in range(target.shape[1]):
#         summ = 0
#         for i in range(target.shape[1]):
#             if i != j:
#                 summ = summ + (1 - p[i, j])
#         g[0, j] = (1/(target.shape[1] - 1))*p[j, j]*summ
#
#     return g
#
#
# def g_ordering(g):
#     g_class = []
#     for f in range(g[0].shape[1]):
#         g_temp = np.zeros((1, len(g)))
#         for i in range(len(g)):
#             g_temp[0, i] = g[i][0, f]
#         g_class.append(g_temp)
#
#     return g_class
#
#
# # Function to evaluate fuzzy integral.
# def fuzzy_integral(g, h):
#     g_class = g_ordering(g)
#     n_classes = len(g_class)
#     n_nn = len(g)
#     lambda_net = []
#     for i in range(n_classes):
#         lambda_net.append(lambda_calc(g_class[i]))
#
#     fi = np.zeros((len(g), 2))
#
#     fusion = np.zeros((1, n_classes))
#     for i in range(n_classes):
#         for f in range(n_nn):
#             fi[f, 0] = h[f][0, i]
#             fi[f, 1] = g[f][0, i]
#
#         fi_sorted = fi[fi[:, 0].argsort()[::-1]]
#         for f in range(n_nn):
#             if f > 0:
#                 fi_sorted[f, 1] = fi_sorted[f, 1] + fi_sorted[f - 1, 1] + fi_sorted[f, 1] * fi_sorted[f - 1, 1] * \
#                                                                           lambda_net[i]
#         fusion[0, i] = np.amax(np.amin(fi_sorted, axis=1))
#
#     classification = (fusion == fusion.max(axis=1)).astype(int)
#
#     return fusion, classification
#
#
# # Function to calculate neural network accuracy
# def network_accuracy(value, target):
#     accuracy = np.zeros(value.shape)
#     accuracy[(value[:, 0:value.shape[1]] == target[:, 0:value.shape[1]])] = 5
#
#     sum_accuracy = np.sum(accuracy, axis=1)
#     sum_accuracy[(sum_accuracy != 5 * value.shape[1])] = 0
#     sum_accuracy[sum_accuracy == 5 * value.shape[1]] = 1
#
#     return np.sum(sum_accuracy)*100/value.shape[0]
#
#
# def log_data(data_to_log, name):
#
#     list_length = len(data_to_log)
#     for i in range(list_length):
#         os.makedirs(name, exist_ok=True)
#         file_name = 'C:/temp/restore_test/' + name + '/' + name + str(i + 1)
#         np.save(file_name, data_to_log[i])
#
#
# def load_data(name_data, number_files):
#     list_data = []
#     for i in range(number_files):
#         name_file = name_data + '/' + name_data + str(i+1) + '.npy'
#         # name_file += str(i+1)
#         # name_file += '.npy'
#         list_data.append(np.load(name_file))
#
#     return list_data
#
#
# def main():
#
#     n_run = 5
#     n_fnfn = 3
#     individual_accuracies = np.zeros((n_run, n_fnfn))
#     fusion_accuracy = np.zeros((n_run, 1))
#
#     weights_net = []
#     mean_net = []
#     variance_net = []
#     g_net = []
#     for r in range(n_run):
#         # weights_net = []
#         # mean_net = []
#         # variance_net = []
#         # g_net = []
#
#         train_inputs, train_labels_one_hot, test_inputs, test_labels_one_hot = load_iris()
#         for i in range(n_fnfn):
#             weights, mean, variance = structure_learning(train_inputs, train_labels_one_hot)
#             weights_net.append(weights)
#             mean_net.append(mean)
#             variance_net.append(variance)
#
#             d_input_0 = train_inputs.shape[0]
#             d_inputs_1 = train_inputs.shape[1]
#             n_outputs = train_labels_one_hot.shape[1]
#             results = np.zeros((d_input_0, 2*n_outputs))
#
#             for h in range(d_input_0):
#                 results[h, 0:n_outputs] = evaluate_fnfn(np.reshape(train_inputs[h], (d_inputs_1, 1)),
#                                                         weights_net[r*n_fnfn + i], mean_net[r*n_fnfn + i],
#                                                         variance_net[r*n_fnfn + i])
#                 results[h, n_outputs:2*n_outputs] = train_labels_one_hot[h]
#
#             results = np.absolute(np.rint(results))
#             accuracy = np.zeros((d_input_0, n_outputs))
#             accuracy[(results[:, 0:n_outputs] == results[:, n_outputs:2*n_outputs])] = 5
#
#             g_net.append(0.5*calc_g(results[:, 0:n_outputs], train_labels_one_hot))
#
#         d_input_0 = test_inputs.shape[0]
#         d_inputs_1 = test_inputs.shape[1]
#         n_outputs = test_labels_one_hot.shape[1]
#         results3 = np.zeros((d_input_0, (1 + n_fnfn) * n_outputs))
#         networks_results = np.zeros((d_input_0, n_fnfn * n_outputs))
#         fusion = np.zeros((d_input_0,  n_outputs))
#         classification = np.zeros((d_input_0, n_outputs))
#
#         for h in range(d_input_0):
#             h_net = []
#             for i in range(n_fnfn):
#                 results3[h, i*n_outputs:(i+1)*n_outputs] = evaluate_fnfn(np.reshape(test_inputs[h], (d_inputs_1, 1)),
#                                                                          weights_net[r*n_fnfn + i], mean_net[r*n_fnfn +
#                                                                          i], variance_net[r*n_fnfn + i])
#                 networks_results[h, i*n_outputs:(i+1)*n_outputs] = (results3[h, i*n_outputs:(i+1)*n_outputs] ==
#                                                                     results3[h, i*n_outputs:(i+1)*n_outputs].max()
#                                                                     ).astype(int)
#                 h_net.append(np.reshape(results3[h, i*n_outputs:(i+1)*n_outputs], (1, 3)))
#             results3[h, n_fnfn*n_outputs:(1 + n_fnfn) * n_outputs] = test_labels_one_hot[h]
#             fusion[h, 0:n_outputs], classification[h, 0:n_outputs] = fuzzy_integral(g_net[r*n_fnfn:(r+1)*n_fnfn], h_net)
#
#         # print(networks_results)
#         print(g_net)
#         # print(results3)
#         print(fusion)
#
#         for i in range(n_fnfn):
#             individual_accuracies[r, i] = network_accuracy(networks_results[:, i*n_outputs:(i+1)*n_outputs],
#                                                            results3[:, n_fnfn * n_outputs:(1 + n_fnfn)*n_outputs])
#
#         fusion_accuracy[r, 0] = network_accuracy(classification, results3[:, n_fnfn*n_outputs:(1 + n_fnfn)*n_outputs])
#
#         # Plot data
#         # Plot FNFN1
#         plt.ion()
#         plt.show()
#         plt.figure(r)
#         ax1 = plt.subplot(511)
#         plt.scatter(range(1, 76), np.argmax(networks_results[:, 0:3], axis=1))
#         # Plot FNFN2
#         ax2 = plt.subplot(512, sharex=ax1, sharey=ax1)
#         plt.scatter(range(1, 76), np.argmax(networks_results[:, 3:6], axis=1))
#         # Plot FNFN3
#         ax3 = plt.subplot(513, sharex=ax1, sharey=ax1)
#         plt.scatter(range(1, 76), np.argmax(networks_results[:, 6:9], axis=1))
#         # # Plot FNFN4
#         # ax4 = plt.subplot(514, sharex=ax1, sharey=ax1)
#         # plt.scatter(range(1, 76), np.argmax(networks_results[:, 9:12], axis=1))
#         # Plot MFNFN
#         ax4 = plt.subplot(515, sharex=ax1, sharey=ax1)
#         plt.scatter(range(1, 76), np.argmax(classification[:, 0:3], axis=1))
#
#         plt.draw()
#         plt.xlim(0, 76.0)
#         plt.pause(0.001)
#         plt.show()
#
#         print(individual_accuracies)
#         print(fusion_accuracy)
#
#         if r == n_run - 1:
#             while True:
#                 plt.pause(1)
#
#     # log_data(variance_net, 'variance')
#     # test = load_data('variance',15)
#     # print(variance_net[12])
#     # print(test[12])
#
#
# if __name__ == '__main__':
#     main()

import numpy as np

class  RBM:
    # RBM Restricted Boltzmann Machine Model
    #     RBM implements AbstractNet for Restricted Boltzmann Machines with binary units on both visible and hidden units.
    #     Pretraining regularization includes L2 and L1 weight decay, dropout and hidden units sparsity.

    # author: Nicolas Granger < nicolas.granger @ telecom - sudparis.eu >
    # licence: MIT



    # Constructor - ------------------------------------------------------

    def __init__(self, nVis, nHid, pretrainOpts, trainOpts):
        # RBM Construct a Restricted Boltzmann Machine implementation
        #     obj = RBM(nVis, nHid, pretrainOpts, trainOpts) returns an
        #     RBM with nVis binary input units, nHid binary output units.
        #     The structure pretrainOpts contains pretraining parameters
        #     in the following fields:
        #         lRate       -- learning rate
        #         batchSz     -- # of samples per batch
        #         momentum    -- gradient momentum
        #         sampleVis   -- sample visible units for GS (default:
        #                        false)
        #         sampleHid   -- sample hidden units for GS (default:
        #                        true)
        #         nGS         -- # GS iterations for CD(k)
        #         wPenalty    -- weight penalty [optional]
        #         wDecayDelay -- first epoch with weight decay [optional]
        #         dropHid     -- hidden units dropout rate [optional]
        #         dropVis     -- hidden units dropout rate [optional]
        #         sparsity    -- sparseness objective [optional]
        #         sparseGain  -- learning rate gain for sparsity [optional]
        #
        #         Similary, trainOpts support the following fields:
        #             lRate       -- learning rate
        #             nIter       -- number of gradient iterations
        #             momentum    -- gradient moementum
        #             batchSz = 100;         % # of training points per batch
        #                 lRate     -- coefficient on the gradient update
        #                 decayNorm -- weight decay penalty norm (1 or 2)
        #                 decayRate -- penalty on the weights
        #
        #                 batchSz   -- size of the batches [optional]
        #                 lRate     -- coefficient on the gradient update
        #                 decayNorm -- weight decay penalty norm (1 or 2)
        #                 decayRate -- penalty on the weights

        self.nVis = nVis # number of visible units (dimensions)
        self.nHid = nHid # number of hidden units
        # self.pretrainOpts = {}
        # self.trainOpts = {}

        if 'nGS' not in pretrainOpts:
            pretrainOpts.nGS = 1

        if 'dropVis' not in pretrainOpts:
            pretrainOpts.dropVis = 0

        if 'dropHid' not in pretrainOpts:
            pretrainOpts.dropHid = 0

        if 'sampleVis' not in pretrainOpts:
            pretrainOpts.sampleVis = False

        if 'sampleHid' not in pretrainOpts:
            pretrainOpts.sampleHid = True

        if 'momentum' not in pretrainOpts:
            pretrainOpts.momentum = 0

        self.pretrainOpts = dict(pretrainOpts)

        if 'decayNorm' not in trainOpts:
            trainOpts.decayNorm = -1

        self.trainOpts = dict(trainOpts)

        # Initializing weights 'Ã  la Bengio'
        range = np.sqrt(6 / (2 * self.nVis))
        self.W = 2 * range * (np.random.uniform(size=(nVis, nHid)) - .5) # connection weights
        self.b = np.zeros(nVis, 1) # visible unit biases
        self.c = np.zeros(nHid, 1) # hidden unit biases


    # AbstractNet implementation - ---------------------------------------

    def insize(self):
        return self.nVis


    def outsize(self):
        return self.nHid


    def compute(self, X):
    # [Y, A] = compute(self, X)
        if nargout == 2 and ('dropout' in self.trainOpts):
            A.mask = rand(self.nVis, 1) > self.trainOpts["dropout"]
            X = bsxfun( @ times, X, A.mask. / (1 - self.trainOpts["dropout"]))

            Y = self.vis2hid(X)
            if nargout > 1:
                A.x = X
                A.s = Y


    def pretrain(self, X):
        nObs = X.shape[1]
        opts = dict(self.pretrainOpts)
        dWold = np.zeros(self.W.shape)
        dbold = np.zeros(self.b.shape)
        dcold = np.zeros(self.c.shape)
        act = np.zeros(self.nHid, 1) # mean activity of hidden units

        for e in range(opts["nEpochs"]):
        shuffle = randperm(nObs)

            # Batch loop
            for batchBeg in range(0,nObs,opts["batchSz"]):
                bind = shuffle[batchBeg: min(nObs,batchBeg + opts["batchSz"] - 1)]

                # Gibbs sampling
                dW, db, dc, hid = self.cd(X[:,bind])

                # Activity estimation(Hinton 2010)
                if ('sparsity' in opts) and (e > opts["wDecayDelay"]):
                    act = .9 * act + .1 * mean(hid, 2)

                # Hidden layer selectivity
                if ('selectivity' in opts) and (e > opts["wDecayDelay"]):
                    err = mean(hid, 1) - opts["selectivity"]
                    ds = bsxfun( @ times, hid. * (1 - hid), err);
                    dW = dW + opts["selectivityGain"] * X(:, bind) *ds' / nObs
                    dc = dc + mean(ds, 2)


                # Weight decay
                if ('wPenalty' in opts) and e > opts["wDecayDelay"]:
                    dW = dW + opts["wPenalty"] * self.W
                    db = db + opts["wPenalty"] * self.b
                    dc = dc + opts["wPenalty"] * self.c


                # Momentum
                dW = dWold * opts["momentum"] + (1 - opts["momentum"]) * dW
                db = dbold * opts["momentum"] + (1 - opts["momentum"]) * db
                dc = dcold * opts["momentum"] + (1 - opts["momentum"]) * dc

                # Apply gradient
                self.W = self.W - opts["lRate"] * dW
                self.b = self.b - opts["lRate"] * db
                self.c = self.c - opts["lRate"] * dc

                # Save gradient
                dWold = dW
                dbold = db
                dcold = dc


            # Unit - wise sparsity(Hinton 2010)
            if ('sparsity' in opts) and (e > opts["wDecayDelay"]):
                dc = opts["lRate"] * opts["sparseGain"] * (act - opts["sparsity"])
                self.W = bsxfun( @ minus, self.W, dc')
                self.c = self.c - dc


            # Report
            if ('displayEvery' in opts) and mod(e, opts["displayEvery"]) == 0:
                # Reconstruct input samples
                R = self.hid2vis(self.vis2hid(X))
                # Mean square reconstruction error
                msre = sqrt(mean(mean((R - X). ^ 2)))
                fprintf('%03d , msre = %f\n', e, msre)


    def backprop(self, A, outErr):
        # function[G, inErr] = backprop(self, A, outErr)
        # backprop implementation of AbstractNet.backprop
        # inErr = backprop(self, A, outErr, opts)
        #
        # A      -- forward pass data as return by compute
        # outErr -- network output error derivative w.r.t. output
        #           neurons stimulation
        #
        # inErr  -- network output error derivative w.r.t. neurons
        #           outputs (not activation)

        # Gradient computation
        ds = A.s. * (1 - A.s)
        delta = (outErr. * ds)
        G.dW = A.x * delta'
        G.dc = sum(delta, 2)

        # Error backpropagation
        inErr = self.W * delta

        # Dropout
        if 'dropout' in self.trainOpts:
            G.dW = bsxfun( @ times, G.dW, A.mask)
            inErr = bsxfun( @ times, inErr, A.mask)


    def gradientupdate(self, G):
        opts = dict(self.trainOpts)
        # Gradient update
        self.W = self.W - opts["lRate"] * G.dW
        self.c = self.c - opts["lRate"] * G.dc

        # Weight decay
        if opts["decayNorm"] == 2:
            self.W = self.W - opts["lRate"] * opts["decayRate"] * self.W
            self.c = self.c - opts["lRate"] * opts["decayRate"] * self.c
        elif opts["decayNorm"] == 1:
            self.W = self.W - opts["lRate"] * opts["decayRate"] * sign(self.W)
            self.c = self.c - opts["lRate"] * opts["decayRate"] * sign(self.c)


    # Methods - ----------------------------------------------------------

    def vis2hid(self, X):
        return RBM.sigmoid(bsxfun( @ plus, (X' * self.W)', self.c))

    def hid2vis(self, H):
        return RBM.sigmoid(bsxfun( @ plus, self.W * H, self.b))


    def cd(self, X):
    # function[dW, db, dc, hid0] = cd(self, X)
        # CD Contrastive divergence (Hinton's CD(k))
        # [dW, db, dc, act] = cd(self, X) returns the gradients of
        # the weights, visible and hidden biases using Hinton's
        # approximated CD. The sum of the average hidden units
        # activity is returned in act as well.

        opts = dict(self.pretrainOpts)
        nObs = X.shape[1]
        vis0 = X
        hid0 = self.vis2hid(vis0)

        # Dropout masks
        if opts["dropHid"] > 0:
            hmask = np.random.uniform(size=hid0.shape) < opts["dropHid"]

        if opts["dropVis"] > 0:
            vmask = np.random.uniform(size=X.shape) < opts["dropHid"] # isn't it supposed to be dropVis??

        hid = hid0
        for k in range(opts["nGS"]):
            if opts["sampleHid"]: # sampling ?
                hid = hid > rand(size(hid))

            if (opts["dropHid"] > 0) and (k < opts["nGS"]): # Dropout?
                hid = hid. * hmask / (1 - opts["dropHid"])

            vis = self.hid2vis(hid)

            if (opts["sampleVis"]) and (k < opts["nGS"]): # sampling ?
                vis = vis > rand(size(vis))

            if (opts["dropVis"] > 0) and (k < opts["nGS"]): # Dropout?
                vis = vis.* vmask / (1 - opts["dropVis"])

            # TODO keep non masked visibles for CD but mask for hid computation.

            hid = self.vis2hid(vis)

        dW      = - (vis0 * hid0' - vis * hid') / nObs
        dc      = - (sum(hid0, 2) - sum(hid, 2)) / nObs
        db      = - (sum(vis0, 2) - sum(vis, 2)) / nObs

        return dW, db, dc, hid0


    def sigmoid(X):
        return 1. / (1 + np.exp(-X))


